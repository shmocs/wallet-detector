# wallet-detector: Get all wallet addresses from a known wallet address

> *NOTE*:
> only works with insight explorer

Have an address in exchange X ?
Starting from there, if you have at least one deposit into that address - you can determine all addresses of that exchange wallet. 

#### Usage Notice
This is gamma software. You know...

#### Requirements
* [Node.js](http://nodejs.org/) v14.15 ([follow these installation instructions](https://github.com/nvm-sh/nvm))

##### Config
Take a look at the example json file config.example.json
and replace existing level1 keys 

```bash
    // false to grab all transactions
    // true to only grab latest 50 transactions for running second/third time the bot to refresh addresses
    "onlyRefreshLastTxs": true,
    
    // enough for grabbing only latest 1000 transactions of current address
    "maxTxsLookup": 500,

    "explorer": "https://explorer.vidulum.app",
    "itemsPerPage": 50,

    // get some sleep between api calls to not trigger any flags
    "sleepBetweenCalls": 1,
    "wallet": {
        "coin": "vdl",
        "target": "STEX",
        
        // address that has at least 1 deposit in targeted wallet
        "knownAddress": "v1WMM3Q44yqR5zJzm9nALp7YWN4E546k4f4",
        
        // where to save all found addresses (1 per line)
        "walletAddressesFile": "vidulum-stex.log"
    },
```


##### Install
```bash
git clone git@gitlab.com:shmocs/wallet-detector.git
cd wallet-detector
cp config.example.json config.json

npm install
```
Run
```bash
node index.js
```
Should output:
```bash
##############################################################
[Wallet detector] started at [ 2020-06-15T10:32:13.478Z ]
##############################################################

Parsing [ STEX ] into [ results/vidulum-stex.log ]
Starting with [ 4198 ] addresses in wallet [ STEX ]
Parsed already [ 553 ] addresses of [ STEX ]
Starting from [ {"address":"v1WMM3Q44yqR5zJzm9nALp7YWN4E546k4f4","from":550,"to":600} ]
```
