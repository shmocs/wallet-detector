const fs = require('fs')
const axios = require('axios')

module.exports = function (botConfig) {

	const _this = this;

	let walletAddressesFile = ''
	let parsedAddressesFile = ''
	let currentPositionFile = ''

	let walletAddresses = []
	let parsedAddresses = []

	this.getWalletAddresses = function (file) {
		walletAddressesFile = file

		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, '')
		} else {
			walletAddresses = fs.readFileSync(file, "utf-8").split('\n')
			console.log('Starting with [', (walletAddresses.length - 1) ,'] addresses in wallet [', botConfig.wallet.target ,']')
		}

		return walletAddresses
	}
	// save address into the wallet log file & memory
	this.saveWalletAddress = function (addr) {
		walletAddresses.push(addr)

		fs.appendFileSync(walletAddressesFile, addr + '\n')
		console.log('Adding address [', addr ,'] to wallet')
	}

	this.getParsedAddresses = function (file) {
		parsedAddressesFile = file

		if (!fs.existsSync(file)) {
			fs.writeFileSync(file, '')
		} else {
			parsedAddresses = fs.readFileSync(file, "utf-8").split('\n')
			console.log('Parsed already [', (parsedAddresses.length - 1) ,'] addresses of [', botConfig.wallet.target ,']')
		}

		return parsedAddresses
	}
	// save address into the wallet log file
	this.saveParsedAddress = function (addr) {
		parsedAddresses.push(addr)

		fs.appendFileSync(parsedAddressesFile, addr + '\n')
		console.log('Adding address [', addr ,'] to parsed list')
	}


	// get address + tx to start from that point
	this.getCurrentPosition = function (file) {
		currentPositionFile = file
		let currentPosition = {address: botConfig.wallet.knownAddress, txid: null, from: 0, to: botConfig.itemsPerPage}

		if (!fs.existsSync(currentPositionFile)) {
			fs.writeFileSync(currentPositionFile, JSON.stringify(currentPosition))
			console.log('Starting from initial known address [', botConfig.wallet.knownAddress ,']')
		} else {
			currentPosition = JSON.parse(fs.readFileSync(currentPositionFile, "utf-8"))
			console.log('Starting from [', JSON.stringify(currentPosition) ,']')
		}

		return currentPosition
	}
	// save address + tx so that future executions will start from this point
	this.updatePosition = function (currentPosition) {
		fs.writeFileSync(currentPositionFile, JSON.stringify(currentPosition))
		console.log('Updating current position [', JSON.stringify(currentPosition) ,']')
	}

	// save parsed addresses
	this.updateParsed = function () {
		fs.appendFileSync(parsedAddressesFile, JSON.stringify(currentPosition))
		console.log('Updating current position [', JSON.stringify(currentPosition) ,']')
	}


	/* {
		"totalItems": 3061,
		"from": 0,
		"to": 50,
		"items": [
			{
				"txid": "4b59ddc35b6b20ad87b5930b5de247c5eb33426baf2bc4dd10f1983e173fcbc9",
				"version": 4,
				"locktime": 1291037,
				"vin": [...]
	* */
	// https://explorer.snowgem.org/api/addrs/s1fdenmcW47fNgq9WBApBZPTRjtDLBunRX1,s1WRdsFoCSbLa4FZ89XrSnfxrbN2BfQK7Mg,s1hndXbXsTvpxCehWqZTKwmk8Hhup8odUhx,s1aFatKQVKGKb6fdzsUuEpGhXUFK4Y9G44m,s1dq3JP2L5Vp8JBsygWWvmR5sascsy8jnpc/txs?from=0&to=50
	this.getAddressTxs = function (address, from = 0, to = 50, callback) {
		if (!address || address === undefined) return

		setTimeout(() => {
			console.log('\nReqesting: /api/addrs/' + address + '/txs?from=' + from + '&to=' + to)

			axios.get(botConfig.explorer + '/api/addrs/' + address + '/txs?from=' + from + '&to=' + to)
			.then(function (response) {
				// handle success
				//console.log(response.data);

				if (response && response.data) {
					callback(address, response.data)
				}
			})
			.catch(function (error) {
				// handle error
				console.log(error);
			})
			.then(function () {
				// always executed
			})

		}, botConfig.sleepBetweenCalls * 1000)
	}

	this.parseTxs = function (currentAddress, items) {
		// look for siblings in vin[] where currentAddress EXISTS in the vin inputs
		const filteredItems = items.filter(item => {
			return item.vin.find(_in => _in.addr === currentAddress)
		})
		//console.log(filteredItems)

		filteredItems.map(item => {
			item.vin.map(_in => {
				if (!walletAddresses.includes(_in.addr)) {
					// update wallet file on disk
					_this.saveWalletAddress(_in.addr)
				}
			})
		})
	}

	this.nextAddress = function (address, callback) {
		if (!parsedAddresses.includes(address)) {
			_this.saveParsedAddress(address)
		}

		// get next address from the walletAddresses that is not parsed already
		let difference = walletAddresses.filter(x => !parsedAddresses.includes(x))

		if (difference.length) {
			const nextAddr = difference.shift()
			_this.getAddressTxs(nextAddr, 0, botConfig.itemsPerPage, callback)
		}

		console.log('difference.length: ', difference.length)
	}

}