const fs = require('fs')
const Utils = require('./libs/utils.js');

const now = new Date()
console.log('##############################################################')
console.log('[Wallet detector] started at [', now.toISOString() , ']')
console.log('##############################################################\n')

const configFileName = 'config.json'
if (!fs.existsSync(configFileName)) {
	console.log('Config [' + configFileName + '] file does not exist.')
	return
}
const botConfig = JSON.parse(fs.readFileSync(configFileName, "utf-8"))
const utils = new Utils(botConfig)
const resultsDir = 'results'

// make sure output folder for results exists
if (!fs.existsSync(resultsDir)) {
	try { fs.mkdirSync(resultsDir) } catch(e) { throw e }
}

const walletAddressesFile = resultsDir + '/' + botConfig.wallet.walletAddressesFile;
console.log('Parsing [', botConfig.wallet.target ,'] into [', walletAddressesFile ,']')

const currentPositionFile = resultsDir + '/position_' + botConfig.wallet.coin + '_' + botConfig.wallet.target + '.log'
const parsedAddressesFile = resultsDir + '/parsed_' + botConfig.wallet.coin + '_' + botConfig.wallet.target + '.log'

let walletAddresses = utils.getWalletAddresses(walletAddressesFile)
let parsedAddress = utils.getParsedAddresses(parsedAddressesFile)
let currentPosition = utils.getCurrentPosition(currentPositionFile)


const handleMultipleTxs = function (address, txs) {
	console.log(`Getting [${txs.to - txs.from}]-[${txs.from}, ${txs.to}] txs from [${txs.totalItems}] total for [${address}]`)
	//console.log(txs)

	utils.parseTxs(address, txs.items)

	// for periodical refresh of wallet addresses just
	// 		- set onlyRefreshLastTxs = true in config
	// 		- empty parsed_coin_TARGET.log
	// so it will get only first 0-50 txs for an address
	if (txs.to < txs.totalItems && txs.to < botConfig.maxTxsLookup && botConfig.onlyRefreshLastTxs === false) {
		utils.getAddressTxs(address, txs.to, txs.to + botConfig.itemsPerPage, handleMultipleTxs)
	} else {
		utils.nextAddress(address, handleMultipleTxs)
	}

	utils.updatePosition({
		address: address,
		from: txs.to,
		to: txs.to + botConfig.itemsPerPage
	})
}

utils.getAddressTxs(currentPosition.address, currentPosition.from, currentPosition.to, handleMultipleTxs)